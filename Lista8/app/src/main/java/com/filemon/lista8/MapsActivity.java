package com.filemon.lista8;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Button ButtonZad1;
    private Button ButtonZad2;
    private Button ButtonZad3;
    private Button ButtonZad4;
    private Context self = this;
    private LocationManager locationManager;
    private LatLng uniWroc = new LatLng(51.1163217,17.02805);
    private LatLng mieszkanie = new LatLng(51.1046259,17.0246143);
    private boolean isLocationEnabled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationManager = (LocationManager) (this).getSystemService(Context.LOCATION_SERVICE);
        isLocationEnabled =locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);





        ButtonZad1 = (Button) findViewById(R.id.buttonZad1);
        ButtonZad2 = (Button) findViewById(R.id.buttonZad2);
        ButtonZad3 = (Button) findViewById(R.id.buttonZad3);
        ButtonZad4 = (Button) findViewById(R.id.buttonZad4);

        ButtonZad1.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Geocoder geocoder = new Geocoder(self, Locale.getDefault());
                double latitude=49.6923816;
                double longitude=21.6844794;
                List<Address> addresses = null;
                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                String address = addresses.get(0).getAddressLine(0);
                String text = "Address:\n"+address;
                initiatePopupWindow(findViewById(android.R.id.content), text);
            }
        });


        ButtonZad2.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Location uniWrocLoc = new Location("Wydział WfA");//provider name is unnecessary
                uniWrocLoc.setLatitude(51.1163217);
                uniWrocLoc.setLongitude(17.02805);
                Location mieszkanieLoc = new Location("Mieszkanie");//provider name is unnecessary
                mieszkanieLoc.setLatitude(51.1046259);
                mieszkanieLoc.setLongitude(17.0246143);



                String text = "Distance: "+String.valueOf(uniWrocLoc.distanceTo(mieszkanieLoc))+" meters";
                initiatePopupWindow(findViewById(android.R.id.content), text);
            }
        });

        ButtonZad4.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Location uniWrocLoc = new Location("Wydział WfA");//provider name is unnecessary
                uniWrocLoc.setLatitude(0);
                uniWrocLoc.setLongitude(180);
                Location mieszkanieLoc = new Location("Mieszkanie");//provider name is unnecessary
                mieszkanieLoc.setLatitude(0);
                mieszkanieLoc.setLongitude(0);


                String text = "Distance: "+String.valueOf((uniWrocLoc.distanceTo(mieszkanieLoc)*2)/1000)+" km";
                initiatePopupWindow(findViewById(android.R.id.content), text);
            }
        });
    }

    private void initiatePopupWindow(View v,String text) {
        try {
            //We need to get the instance of the LayoutInflater, use the context of this activity
            LayoutInflater inflater = (LayoutInflater) MapsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //Inflate the view from a predefined XML layout
            View layout = inflater.inflate(R.layout.popup, (ViewGroup) findViewById(R.id.linLay));
            // create a 300px width and 470px height PopupWindow
            final PopupWindow pw = new PopupWindow(layout,ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);

            TextView address = (TextView) layout.findViewById(R.id.textView);
            address.setText(text);
            // display the popup in the center
            pw.showAtLocation(v, Gravity.CENTER, 0, 0);
            pw.getContentView().setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v) {
                    pw.dismiss();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        mMap.addMarker(new MarkerOptions().position(uniWroc).title("uniWroc"));
        mMap.addMarker(new MarkerOptions().position(mieszkanie).title("mieszkanie"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(uniWroc,14));

    }

}

