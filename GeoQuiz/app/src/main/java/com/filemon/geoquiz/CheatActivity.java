package com.filemon.geoquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class CheatActivity extends AppCompatActivity {


    private Button mCheckButton;
    private TextView showAnswer;
    private Button mBackButton;
    private boolean cheater = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);
        final boolean answer = getIntent().getBooleanExtra("Answer",false);


        showAnswer = (TextView) findViewById(R.id.cheatAnswer);
        mCheckButton = (Button) findViewById(R.id.checkAnswer);
        mCheckButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showAnswer.setText(String.valueOf(answer));
                        cheater = true;
                    }
                }
        );

        mBackButton = (Button) findViewById(R.id.backButton);
        mBackButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                closeActivity();
            }
        });



    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        Intent returnIntent = new Intent();
        if(cheater) setResult(MainActivity.RESULT_OK,returnIntent);
        else setResult(MainActivity.RESULT_CANCELED,returnIntent);
        finish();
    }

}
