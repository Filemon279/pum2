package com.filemon.geoquiz;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class QuestionFragment extends Fragment {
    private TextView mQuestionTextView;
    private static final String ARG_CRIME_ID = "crime_id";
    private static QuestionBank mQuestionsBank = QuestionBank.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_question, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mQuestionTextView = (TextView) view.findViewById(R.id.QuestionText);

        int questionID = (int) getArguments().getSerializable(ARG_CRIME_ID);

        String question = mQuestionsBank.getQuestion(questionID).getTextResId();
        mQuestionTextView.setText(question);
    }

    public static QuestionFragment newInstance(int questionID) {
        Bundle args = new Bundle();
        args.putInt(ARG_CRIME_ID, questionID);

        QuestionFragment fragment = new QuestionFragment();
        fragment.setArguments(args);
        return fragment;
    }


}