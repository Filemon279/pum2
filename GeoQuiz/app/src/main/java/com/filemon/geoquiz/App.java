package com.filemon.geoquiz;

import android.app.Application;
import android.content.Context;

/**
 * Created by Filemon on 1/14/2018.
 */

public class App extends Application {
    private static Context appContext;
    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
    }
    public static Context getAppContext() {
        return appContext;
    }
}