package com.filemon.geoquiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Summary extends AppCompatActivity {

    TextView pointsField;
    TextView tokenField;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);


        String points = getIntent().getStringExtra("POINTS");
        String tokens = getIntent().getStringExtra("TOKENS");

        pointsField = (TextView) findViewById(R.id.points);
        tokenField = (TextView) findViewById(R.id.tokens);

        pointsField.setText(points);
        tokenField.setText(tokens);
    }
}
