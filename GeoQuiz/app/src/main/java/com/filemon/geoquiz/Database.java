package com.filemon.geoquiz;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Filemon on 1/14/2018.
 */

public class Database extends SQLiteOpenHelper {

    public Database(Context context)
    {
        super(context, "questions.db",null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE questions(id INTEGER PRIMARY KEY AUTOINCREMENT, question TEXT NOT NULL, answer INTEGER DEFAULT 0);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

    }

    public void addQuestion(String question, int answer)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues data = new ContentValues();
        data.put("question",question);
        data.put("answer",answer);
        db.insertOrThrow("questions",null,data);
    }

    public void removeQuestion(String question)
    {
        SQLiteDatabase db = getWritableDatabase();
        db.delete("questions","question=?",new String[]{question});
    }


    public Cursor getAllQuestions()
    {
        String[] colums = {"id","question","answer"};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("questions",colums,null,null,null,null,null);
        return cursor;
    }


}
