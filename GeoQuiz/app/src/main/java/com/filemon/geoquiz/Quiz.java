package com.filemon.geoquiz;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;


public class Quiz extends Fragment {


    private ImageButton mTrueButton;
    private ImageButton mFalseButton;
    private ImageButton mNextButton;
    private ImageButton mPrevButton;
    private Button mCheatButton;
    private Button mQList;
    private int Tokens;

    private TextView mApiTextView;
    private TextView tokenInfo;
    private static QuestionBank mQuestionsBank = QuestionBank.getInstance();

    private int mCurrentIndex = 0;
    private ViewPager mViewPager;
    private List<Question> mQuestions;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_quiz, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        mViewPager = (ViewPager) view.findViewById(R.id.vpPager);

        mQuestions = mQuestionsBank.getQuestions();

        FragmentManager fragmentManager = getFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                //  manageAnswerButtons();
                return QuestionFragment.newInstance(position);
            }

            @Override
            public int getCount() {

                return mQuestions.size();
            }

            @Override
            public int getItemPosition(Object object) {
                return POSITION_NONE;
            }

        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {}
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {if(mViewPager.getCurrentItem()!=mCurrentIndex) {mCurrentIndex=mViewPager.getCurrentItem();
                manageAnswerButtons();
            }
            }
            public void onPageSelected(int position) {
            }
        });


        Tokens = 3;
        mApiTextView = (TextView) view.findViewById(R.id.apiTextView);
        tokenInfo = (TextView)view.findViewById(R.id.tokenInfo);
        tokenInfo.setText("TOKENS: "+Tokens);
        mApiTextView.setText("API : "+ Build.VERSION.SDK_INT);



        mCheatButton = (Button) view.findViewById(R.id.cheatButton);
        mCheatButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent cheat = new Intent(getActivity().getApplicationContext(), CheatActivity.class);
                        cheat.putExtra("Answer", mQuestionsBank.getQuestion(mCurrentIndex).isAnswerTrue());
                        startActivityForResult(cheat, 1);
                    }
                }
        );

        mQList = (Button) view.findViewById(R.id.QList);
        mQList.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent cheat = new Intent(getActivity().getApplicationContext(), QuestionListActivity.class);
                        startActivity(cheat);
                    }
                }
        );

        mTrueButton = (ImageButton) view.findViewById(R.id.true_button);
        mTrueButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkAnswer(true);
                    }
                }
        );

        mFalseButton = (ImageButton) view.findViewById(R.id.false_button);
        mFalseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(false);
            }
        });

        mNextButton = (ImageButton) view.findViewById(R.id.next_button);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentIndex = (mCurrentIndex + 1) % mQuestionsBank.size();
                mViewPager.setCurrentItem(mCurrentIndex);
                manageAnswerButtons();
            }
        });

        mPrevButton = (ImageButton) view.findViewById(R.id.prev_button);
        mPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentIndex--;
                if(mCurrentIndex<0) mCurrentIndex = mQuestionsBank.size()-1;
                mViewPager.setCurrentItem(mCurrentIndex);
                manageAnswerButtons();
            }
        });
        //  updateQuestion();



    }

    private void manageAnswerButtons()
    {
        boolean wasAnswered = mQuestionsBank.getQuestion(mCurrentIndex).getAnswered();

        if(wasAnswered)
        {
            mFalseButton.setEnabled(false);
            mFalseButton.setAlpha(0.2f);
            mTrueButton.setEnabled(false);
            mTrueButton.setAlpha(0.2f);
        }
        else
        {
            mFalseButton.setEnabled(true);
            mFalseButton.setAlpha(1f);
            mTrueButton.setEnabled(true);
            mTrueButton.setAlpha(1f);
        }
    }

    private void checkAnswer(boolean userPressedTrue) {
        boolean answerIsTrue = mQuestionsBank.getQuestion(mCurrentIndex).isAnswerTrue();
        mQuestionsBank.getQuestion(mCurrentIndex).setAnswered(userPressedTrue == answerIsTrue);
        manageAnswerButtons();
        int toastMessageId = 0;

        if (userPressedTrue == answerIsTrue) {
            toastMessageId = R.string.correct_toast;

        } else {
            toastMessageId = R.string.incorrect_toast;
        }
        Toast toast = Toast.makeText(getActivity().getApplicationContext(),toastMessageId,Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP,0,0);
        toast.show();

        int points = allAnswered();
        if(points != -1)
        {
            String komunikat = points+"/"+mQuestionsBank.size();
            //mQuestionTextView.setText(komunikat);
            Intent intent = new Intent(this.getContext(), SummaryActivity.class);
            intent.putExtra("POINTS", komunikat);
            intent.putExtra("TOKENS", tokenInfo.getText().toString());
            startActivity(intent);
        }

    }

    private int allAnswered()
    {
        int points = 0;
        for(int i = 0; i<mQuestionsBank.size();i++)
        {
            if(!mQuestionsBank.getQuestion(i).getAnswered()) return -1;
            System.out.println(i+" : "+mQuestionsBank.getQuestion(i).checkPoint());
            points += mQuestionsBank.getQuestion(i).checkPoint() ? 1 : 0;
        }
        return points;

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == MainActivity.RESULT_OK){
                Toast toast = Toast.makeText(getActivity().getApplicationContext(),"CHEATER!!",Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP,0,0);
                toast.show();
                Tokens--;
                tokenInfo.setText("TOKENS: "+Tokens);
                if(Tokens==0) mCheatButton.setVisibility(View.INVISIBLE);
                mQuestionsBank.getQuestion(mCurrentIndex).setCheat();
            }

        }
    }

}