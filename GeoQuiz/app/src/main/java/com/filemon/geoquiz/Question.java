package com.filemon.geoquiz;

/**
 * Created by Filemon on 1/14/2018.
 */

public class Question {

    private String mTextResId;
    private boolean mAnswerTrue;
    private boolean mAnswered;
    private boolean mPoint;
    private boolean mCheater;

    public Question(String textResId, boolean answerTrue)    {

        mTextResId=textResId;
        mAnswerTrue = answerTrue;
        mAnswered = false;
        mPoint = false;
        mCheater = false;
    }

    public String getTextResId() {
        return mTextResId;
    }

    public void setTextResId(String textResId) {
        mTextResId = textResId;
    }

    public boolean isAnswerTrue() {
        return mAnswerTrue;
    }

    public void setAnswered(boolean point)
    {
        mAnswered = true;
        mPoint = point;
    }

    public boolean checkPoint()
    {
        return mPoint;
    }

    public void setCheat()
    {
        mCheater = true;
    }

    public boolean getCheat()
    {
        return mCheater;
    }


    public boolean getAnswered()
    {
        return mAnswered;
    }

    public void setAnswerTrue(boolean answerTrue) {
        mAnswerTrue = answerTrue;
    }
}
