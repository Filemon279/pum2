package com.filemon.geoquiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class SummaryActivity extends AppCompatActivity {

    TextView pointsField;
    TextView tokenField;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        String points = getIntent().getStringExtra("POINTS");
        String tokens = getIntent().getStringExtra("TOKENS");

        pointsField = (TextView) findViewById(R.id.points);
        tokenField = (TextView) findViewById(R.id.tokens);

        pointsField.setText(points);
        tokenField.setText(tokens);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sec, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_back) {
            Intent cheat = new Intent(this, QuestionListActivity.class);
            startActivity(cheat);
        }
        return super.onOptionsItemSelected(item);
    }
}
