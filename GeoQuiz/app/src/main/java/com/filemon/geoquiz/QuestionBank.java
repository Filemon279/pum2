package com.filemon.geoquiz;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Filemon on 1/14/2018.
 */

public class QuestionBank {
    private static QuestionBank ourInstance = new QuestionBank();
    private List<Question> Questions = new ArrayList<Question>();


    private int currentIndex = -1;

    public static QuestionBank getInstance() {
        return ourInstance;
    }

    private QuestionBank() {
        Database db = MainActivity.getDatabase();

        Cursor cursor = db.getAllQuestions();
        while(cursor.moveToNext())
        {
            Boolean ans = false;
            if(cursor.getInt(2)==1) ans=true;
            Questions.add(new Question(cursor.getString(1), ans));
        }
    }

    public void addQuestion(String question, int intAns)
    {
        Boolean ans = false;
        if(intAns==1) ans=true;
        Questions.add(new Question(question, ans));

    }

    public Question getQuestion(int index)
    {
        return Questions.get(index);
    }

    public int size()
    {
        return Questions.size();
    }

    public List<Question> getQuestions()
    {
        return Questions;
    }

}
